package ch.hevs;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author Nathan Gaillard
 * Simple console app that simulates an authentication solution
 */
public class Authenticator {
    private static HashMap<String, String> credentials = new HashMap<>();

    public static void main(String[] args) {
        while (true) {
            System.out.println("Choose a function: ");
            System.out.println("1: Sign in\n2: Sign up");
            Scanner sc = new Scanner(System.in);
            int ans = sc.nextInt();
            switch (ans) {
                case 1 -> signIn();
                case 2 -> signUp();
                default -> System.err.println("No such function");
            }
        }
    }

    private static void signIn() {
        Scanner sc = new Scanner(System.in);
        String username;
        String password;
        System.out.println("Enter your username: ");
        username = sc.nextLine();
        if (!credentials.containsKey(username)) {
            System.err.println("Username doesn't exist");
            return;
        }
        System.out.println("Enter your password: ");
        password = sc.nextLine();
        if(password.length()>64){
            password = password.substring(0,64);
        }

        if(credentials.get(username).equals(hash(password))){
            System.out.println("Successful login!");
        }else{
            System.err.println("Incorrect password!");
        }
    }

    private static void signUp() {
        Scanner sc = new Scanner(System.in);
        String username;
        String password;
        System.out.println("Choose a username: ");
        username = sc.nextLine();
        if (credentials.containsKey(username)) {
            System.err.println("Username is already taken");
            return;
        }
        System.out.println("Choose a password: ");
        password = sc.nextLine();
        if(password.length()>64){
            password = password.substring(0,64);
        }

        credentials.put(username, hash(password));
    }

    private static String hash(String password){
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return new String(md.digest(password.getBytes(StandardCharsets.UTF_8)));
    }
}
