# Try to complete a coding challenge with a timing attack
## Answers
### Time complexity of the brute force algorithm
O(n<sup>s</sup>) Where *n* is the number of available characters(here, 26), and *s* the password size(here, 4).

This algorithm takes an average of 26<sup>4</sup> iteration to find the password.

### Limitation in the re-usability of the brute force algorithm
This algorithm is ineffective against normal-sized password, because of the time complexity and the call stack limitation.

- Simple insensitive case ABC, 8 characters => 26<sup>8</sup> = 208,827,064,576.
- UTF-8 Charset, 8 characters => 1,112,064<sup>8</sup> = 2.3390433e+48

### *getStats()* method. Run it a hundred time to check your answer at question 1.
```java
private static void getStats(int nbOfTests) {
    long totalTrials = 0;
    long totalTime = 0;
    for (int i = 0; i < nbOfTests; i++) {
        aliceGame();
        long start = System.currentTimeMillis();
        bruteForceGuess();
        long end = System.currentTimeMillis();
        totalTime += (end - start);
        totalTrials += trials;
    }
    long averageTrials = totalTrials / nbOfTests;
    System.out.println("Average trials: " + averageTrials);
    long averageTime = totalTime / nbOfTests;
    System.out.println("Average time: " + averageTime + "ms");
}
    
public static void main(String[] args) {
        getStats(100);
}
```
```bash
Average trials: 456963
Average time: 7ms
```

### Timing-based algorithm
```java
private static void timeComparisonGuess() {
    // If password has not been found, start again
    while (true) {
        StringBuilder sb = new StringBuilder();
        char[] a = sb.append("A".repeat(PASSWORD_SIZE)).toString().toCharArray();
        for (int i = 0; i < PASSWORD_SIZE; i++) {
            a[i] = getLetter(a, i);
        }
        if(checkPassword(String.valueOf(a))){
            return;
        }
    }
}

// Tries to find with which letter combination the check is the longest
private static char getLetter(char[] known, int pos) {
    long biggestTime = Long.MIN_VALUE;
    char prediction = 'A';
    for (int i = 0; i < ABC.length; i++) {
        known[pos] = ABC[i];
        long start = System.nanoTime();
        checkPassword(String.valueOf(known));
        long end = System.nanoTime();
        if (end - start > biggestTime) {
            biggestTime = end - start;
            prediction = ABC[i];
        }
    }
    return prediction;
}
```

### Difficulties, reliability, speed, stats
This algorithm is much more difficult to implement than a recursive algorithm. 
Not reliable because there is too much noise to know if a prediction is correct or not and nanoseconds are not precise enough.

If we implemented a trials limit, the reliability would be insufficient.

It is generally much slower than the brute force solution.
Also, the greater the password size is, the more likely the algorithm had a wrong prediction and has to restart.

```bash
Average trials: 3139227889
Average time:  151471ms
```

### Does a C/C++ implementation solve the issues?
Operating systems and CPUs clocks, like the JVM, have limitations in their time measurement capabilities and there will be too much noise on a personal computer.


