# Try to complete a coding challenge with a timing attack
AliceGame is a Java program that generates a four letters password. The challenge is to write a Java method
that finds the password. A brute force algorithm has successfully completed the challenge. Do the following: 

1. Using your Math and Algorithm courses knowledge, what is the time complexity of the brute force algorithm?
2. Do you see any limitation in the re-usability of the brute force algorithm?
3. Write a *getStats()* method that outputs in the console the average trials and the average time in milliseconds. Run it a hundred time to check your answer at question 1.
4. Try to implement a timing-based algorithm.
5. What difficulties do you encounter? Does it work all the time? Is it faster than the brute force algorithm? Modify the *getStats()* method to see the results.
6. Does an implementation in C/C++ solves your issues? (Hint: Java smallest unit of time is the nanosecond, whereas C/C++ is the picosecond.).

## How to edit
Use your favorite text editor (e.g [Microsoft Windows Notepad](https://apps.microsoft.com/store/detail/windows-notepad/9MSMLRH6LZF3?hl=en-us&gl=US)
or [JetBrains IntelliJ IDEA](https://www.jetbrains.com/idea/))

## How to run

[Java 11+](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html):

`java ./src/AliceGame.java`