import java.security.SecureRandom;

/**
 * @author Nathan
 */
public class AliceGame {

    private final static int PASSWORD_SIZE = 4;
    private final static char[] ABC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    /* NOT ALLOWED TO MODIFY THE CODE OR TO REFERENCE VARIABLES BELOW */
    private static String passwordToGuess;
    private static long trials;

    private static void generatePasswordToGuess(int size) {
        SecureRandom sr = new SecureRandom();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(ABC[sr.nextInt(ABC.length)]);
        }
        passwordToGuess = sb.toString();
    }

    private static void aliceGame() {
        generatePasswordToGuess(PASSWORD_SIZE);
        trials = 0;
        System.out.println("Try to find my secret word using an algorithm!");
    }

    private static boolean checkPassword(String s) {
        trials++;
        boolean found = true;
        for (int i = 0; i < passwordToGuess.length(); i++) {
            if (passwordToGuess.charAt(i) != s.charAt(i)) {
                found = false;
                break;
            }
        }
        if (found) {
            System.out.println("Congratulation you found Alice's secret in " + trials + " trials: " + passwordToGuess);
            return true;
        }
        return false;
    }
    /* NOT ALLOWED TO MODIFY THE CODE OR TO REFERENCE VARIABLES ABOVE*/

    /**
     * Tries all the possibilities recursively
     */
    private static void bruteForceGuess() {
        StringBuilder sb = new StringBuilder();
        String a = sb.append("A".repeat(PASSWORD_SIZE)).toString();
        bruteForceGuessR(a.toCharArray(), 0);
    }

    private static boolean bruteForceGuessR(char[] trial, int depth) {
        for (int i = 0; i < ABC.length; i++) {
            trial[depth] = ABC[i];
            if (depth == PASSWORD_SIZE - 1) {
                if (checkPassword(String.valueOf(trial))) {
                    return true;
                }
            } else {
                bruteForceGuessR(trial, depth + 1);
            }
        }
        return false;
    }

    public static void main(String[] args) {
        aliceGame();
        bruteForceGuess();
    }
}
