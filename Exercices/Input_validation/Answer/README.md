# Bypass a poorly designed regex - Answer

## Part one
As mentionned in the first hint, the application is using the following regex: ".jpg|.png". 
It means that the existence of the char sequence is tested but not its location.

 To bypass such control, you can use double extensions. Here a file named "test.jpg.php" is accepted although it is not a picture.
 
 ## Part two
 
 To prevent double extensions file to be validated, you should ensure the file ends with ".jpg" or ".png". To do this, you can use an end enchor symbolised by a "$".
 
 The new regex is: 
 ```java
  "(.jpg|.png)$"
```  
 