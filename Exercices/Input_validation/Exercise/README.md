# Bypass a poorly designed regex

Your friend who recently started programming has designed a very simple console application that only accepts pictures (png and jpg). 
He challenges you to circumvent his controls and get a non-picture file accepted.

Run the provided jar from the console using:
``` bash
java -jar input_validation.jar
```

And make the application validate a non-picture file !

If needed, hints are provided by the application.

## Steps
1. Get a non-picture file validated.
2. Replace your friend regex with a new one to fix the issue.

## How to run

- [Java 8+](https://www.oracle.com/java/technologies/javase/jdk-relnotes-index.html):

- input_validation.jar