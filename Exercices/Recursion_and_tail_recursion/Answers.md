# Solution

## Part I
2. The program exits with code 0xC00000FD.
3. 0xC00000FD is the stack overflow error. The recursion went too deep.
4.
```C
int sumOfNaturalNumber(int n, int sum){
    if (n == 0){
        return sum;
    }
    return sumOfNaturalNumber(n-1, sum+n);
}
```

## Part II
1. 
```C
unsigned int sumOfNaturalNumber(unsigned int n){
    return n*(n+1)/2;
}
```
2. Knowing the maximum value of a 32-bit integer is 2^32 - 1, and that the formula multiplies N by itself, 
    we can see that an overflow will occur when N is greater or equal to the square root of 2^32 - 1.

```math
Sqrt(2^32 -1) ~~= 65,535
```
3. Compile the code using optimization. 
```bash
gcc ./SumOfNaturalNumbers.c -O2 -o SumOfNaturalNumbers.exe
```
4. The recursive function
