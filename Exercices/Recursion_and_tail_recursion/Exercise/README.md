# Handle a recursive algorithm

Feel free to do this exercise in another programming language.

Do not use loops.

SumOfNaturalNumber is a C program that computes the sum 
of the first N natural numbers. It has been implemented
with recursion. Do the following:

## Part I
1. Read and understand the code.
2. Using the program, try to find the sum of the first 65,200 natural numbers.
3. In case of error, what does the code 0xC00000FD mean? What caused it?
4. Rewrite the function using tail recursion.
5. Did the error still occur? If yes, find out why.

## Part II
Cleaning your room, you stumble across your old Maths book and find this formula:

```math
Sum of N natural numbers = N(N+1) / 2 
```

1. Implement a function that uses this formula in the program. Can you now compute the sum of the first 65,200 natural numbers?

    Hint: Use unsigned int.

2. Using your knowledge, how large can N be before the result is wrong?

3. Can you get to the same N using the original recursive code? Explain how.

4. What function would you use to calculate the sum if N = 70,000?

## How to run
Install a C compiler (e.g. [GNU Compiler Collection](https://gcc.gnu.org/)).

Compile and run the program.
```bash
gcc ./SumOfNaturalNumbers.c -o SumOfNaturalNumbers.exe
./SumOfNaturalNumbers.exe
Enter a number: 65200
...
echo $?
```

