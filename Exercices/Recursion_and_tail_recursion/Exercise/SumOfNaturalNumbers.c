#include <stdlib.h>
#include <stdio.h>

int sumOfNaturalNumber(int n){
    if (n == 0){
        return n;
    }
    return n + sumOfNaturalNumber(n-1);
}

int main(int argc, char **argv) {
    printf("Enter a number: ");
    int n;

    scanf("%d", &n);
    printf("Result is %i", sumOfNaturalNumber(n));
}
