# Experiment secret detections with gitleaks - Answers

1. What type of secret is it ?
An API key

2. Who checked this sercret in the repository ?
benjamin

3. Locate the secret within the project and display it.
The secret is contained within src/main/resources/credentials.json.

    Display it using: 
    ```bash
    cat credentials.json
    ```