# Experiment secret detections with gitleaks

"Gitleaks is a SAST tool for detecting and preventing hardcoded secrets like passwords, api keys, and tokens in git repos. Gitleaks is an easy-to-use, all-in-one solution for detecting secrets, past or present, in your code."[source](https://github.com/zricethezav/gitleaks#:~:text=no%2Dgit%20option.-,Protect,see%20how%20this%20executed%20here) 

## Linux

Prequisites: 
 - Git
 - Golang (for pre-commit section)
 - Internet connection

### Setup
Search for a gitleaks version according to your environment at [gitleaks release folder](https://github.com/zricethezav/gitleaks/releases) and copy the link.

Download it.
```bash
sudo wget https://github.com/zricethezav/gitleaks/releases/download/v8.8.2/gitleaks_8.8.2_linux_x64.tar.gz
```

Unpack the file
```bash
tar -xvf gitleaks_8.8.2_linux_x64.tar.gz
```

Edit permissions.
```bash
sudo chmod +x gitleaks
```

Place it in binaries.
```bash
sudo mv gitleaks /usr/bin
```

Check your installation.
```bash
gitleaks version
```

### Detection
Clone the following repository : "https://gitlab.com/Benjamin_Biollaz/secret-management".
```bash
git clone https://gitlab.com/Benjamin_Biollaz/secret-management.git
```


Run detection within the repository.
```bash
gitleaks detect -v
```

The output should be similar to this:
```bash
    ○
    │╲
    │ ○
    ○ ░
    ░    gitleaks

{
        "Description": "Generic API Key",
        "StartLine": 2,
        "EndLine": 2,
        "StartColumn": 314,
        "EndColumn": 354,
        "Match": "client_secret\":\"8qnMVEoVZur3J2THxsih5l73\"",
        "Secret": "8qnMVEoVZur3J2THxsih5l73",
        "File": "src/main/resources/credentials.json",
        "Commit": "a87131368df2f9583481b7daad9d6e78de415c0a",
        "Entropy": 4.418296,
        "Author": "benjamin",
        "Email": "benjamin.biollaz@students.hevs.ch",
        "Date": "2022-04-26T08:54:15Z",
        "Message": "credentials added",
        "Tags": [],
        "RuleID": "generic-api-key"
}
6:55PM INF scan completed in 95.99765ms
6:55PM WRN leaks found: 1
```

You can save the output in a file with --report-path option.
```bash
gitleaks detect --report-path secret.txt
```

According to this output, answer the following questions.
1. What type of secret is it ?
2. Who checked this sercret in the repository ? 
3. Locate the secret within the project and display it.

### Pre-commit hook

In this section we are configuring a simple pre-commit hook. Pre-commit hooks allow you to perform checks before commiting files. Here we will be checking for secrets only but pre-commit hooks can point out issues in code such as missing semicolons, trailing whitespace, or debug statements.

Golang is needed for this section.

Pre-commit can be downloaded using pip, homebrew or conda. Here only conda installation is described, so refer to [pre-commit installation guide](https://pre-commit.com/#installation) if you want to use pip or homebrew.

<strong>Pre-commit installation with conda</strong>

Search for a MiniConda version according to your environment at [Miniconda installer for linux]( https://docs.conda.io/en/latest/miniconda.html#linux-installers) and copy the link.

Download MiniConda installer.
```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.11.0-Linux-x86_64.sh
```

Run newly downloaded file.
```bash
bash Miniconda3-latest-Linux-x86_64.sh
```

Accept term and conditions.

Initialise miniconda.

Close your prompt and open it again to apply changes.

Download pre-commit hook.
```bash
conda install -c conda-forge pre-commit
```

<strong>Pre-commit configuration</strong>

Then navigate back to "secret-management" repository from earlier.

Create a .pre-commit-config.yaml at the root the repository.
```bash
sudo nano .pre-commit-config.yaml
```

Add the following lines (pre-commit hook from gitleaks).
```bash
-   repo: https://github.com/zricethezav/gitleaks
    rev: v7.5.0
    hooks:
    -   id: gitleaks
```    

Save and exit the file.

Install pre-commit on you repo.
```bash
pre-commit install
```  

Add an RSA key locally.
```bash
sudo nano src/main/resources/rsa.key
``` 

```bash
-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQCqg6TeAsTgfvNZDpoU2nOQ6/7/RsmwV4PQgBar++Y9shL7RdQ6
ZSTzNg70EoHYuhq/jZQC4rSuYio53TFL/6VLXMLM7iYpGUs1jm1edQ3CwfQew+Tl
4gI3dplkOE3sA0lKv9Kl23qDlRTPNqHZtGheAN8pfdBhsf+PRAayERiKnwIDAQAB
AoGANSGiDL+mIyHLp/Nh1Z+gJQc0RM0E6ZrLavuOlPZ/7rd/LKa8k5mSouo0TJ87
sJGE5i0ycpe7nx7dc3xV8Q1uroNkeex/RVa0FSpHZBZ25TSjCL15IV1WO5Td9jwd
EqjbJWvtssns9Q+jgUNAi5TdGMnu7jTX9Dw3C9dvRm3lbHECQQDglbXc8Om06Dvd
/prao0GOFePeses+SvLsbWO2rY1m3wubnUheV1viR7BtFg+z01fckpaFpwHHOU5y
UPlQ0LInAkEAwl2wS+ZPTQBhZVwEmtFBPJcYbVsCR9s1tVB7FWLB5d3Ws77W+yQY
bzyuLayl9+YMnCxHgTq/rahHVPlbEQpGyQJBAMoNp0sZ7Al4jrQRJpziGE6O3y4y
ACVRJPEVEYr+5aLRtQ3SGZPXILGaMd5S5NmyoO+vaoDgWjc1FaC/vS66NC0CQQCh
nwZaFskaXpaekkXB9v8oQvws8uCSn9WpT6MJxXU2j6FZFffuHbWzyeGMWWQpH0Kd
GrTa4N9CqAuovTDcmo8BAkADaE40hEGqWSzMXHFb7DQt0ogzzVzRXfvZGmOi1q+5
7j1M43+wv3kCLRQ24jJD5BYewmdtdN6ro+7jIsgv+UcZ
-----END RSA PRIVATE KEY-----
```
Add this key to git.
```bash
git add src/main/resources/rsa.key
```

Try to commit your changes.
```bash
git commit -m "embedded rsa key"
```

The output should be similar to this: 
```bash
Detect hardcoded secrets.................................................Failed
- hook id: gitleaks
- exit code: 1

INFO[0000] opening .
INFO[0000] scan time: 3 milliseconds 292 microseconds
WARN[0000] leaks found: 1
```

Your commit is blocked because you have an embeddend RSA key. However, you can bypass this control using:
```bash
SKIP=gitleaks git commit -m "skip gitleaks check"
```

## Windows
Go to [gitleaks release folder](https://github.com/zricethezav/gitleaks/releases) and  download a gitleaks version according to your environment.

Unzip the newly downloaded folder.

Add the file "gitleaks.exe" to your path environment variable so you can use it from everywhere.
```bash
set PATH=%PATH%;C:\Users\bbiol\Downloads\gitleaks_8.8.2_windows_x64\
```
This change is only applied to the current command prompt window to avoid filling your path variable in the long run. If you want to add gitleaks to your path permanently, run this command instead.
```bash
setx path "%path%;C:\Users\bbiol\Downloads\gitleaks_8.8.2_windows_x64\"
```

Clone the following repository : "https://gitlab.com/Benjamin_Biollaz/secret-management".
```bash
git clone https://gitlab.com/Benjamin_Biollaz/secret-management.git
```

Run the detection within this repository.

```bash
gitleaks detect -v
```

The output should look as the following.
```bash
○
    │╲
    │ ○
    ○ ░
    ░    gitleaks

{
        "Description": "Generic API Key",
        "StartLine": 2,
        "EndLine": 2,
        "StartColumn": 314,
        "EndColumn": 354,
        "Match": "client_secret\":\"8qnMVEoVZur3J2THxsih5l73\"",
        "Secret": "8qnMVEoVZur3J2THxsih5l73",
        "File": "src/main/resources/credentials.json",
        "Commit": "a87131368df2f9583481b7daad9d6e78de415c0a",
        "Entropy": 4.418296,
        "Author": "benjamin",
        "Email": "benjamin.biollaz@students.hevs.ch",
        "Date": "2022-04-26T08:54:15Z",
        "Message": "credentials added",
        "Tags": [],
        "RuleID": "generic-api-key"
}
←[90m10:54AM←[0m ←[32mINF←[0m scan completed in 69.1162ms
←[90m10:54AM←[0m ←[31mWRN←[0m leaks found: 1
```
1. What type of secret is it ?
2. Who checked this sercret in the repository ? 
3. According to this output, locate the secret within the project.

