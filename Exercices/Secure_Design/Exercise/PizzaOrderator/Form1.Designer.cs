﻿namespace PizzaOrderator
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.totalPriceLabel = new System.Windows.Forms.Label();
            this.itemsList = new System.Windows.Forms.Label();
            this.mozza = new System.Windows.Forms.NumericUpDown();
            this.spinach = new System.Windows.Forms.NumericUpDown();
            this.mushroom = new System.Windows.Forms.NumericUpDown();
            this.ham = new System.Windows.Forms.NumericUpDown();
            this.olives = new System.Windows.Forms.NumericUpDown();
            this.chicken = new System.Windows.Forms.NumericUpDown();
            this.pineapple = new System.Windows.Forms.NumericUpDown();
            this.pepperoni = new System.Windows.Forms.NumericUpDown();
            this.pizzaPrice = new System.Windows.Forms.Label();
            this.addToCartBtn = new System.Windows.Forms.Button();
            this.orderBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.mozza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mushroom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olives)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chicken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pineapple)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pepperoni)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Extra mozzarella cheese";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Extra spinach";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Extra mushroom";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Extra ham";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(295, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Extra pepperoni";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(295, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Extra pineapple";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(295, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Extra chicken";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(295, 159);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Extra olives";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(159, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(202, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Order Margherita (2.50CHF per extra)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(36, 291);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 15);
            this.label10.TabIndex = 9;
            this.label10.Text = "Current order";
            // 
            // totalPriceLabel
            // 
            this.totalPriceLabel.AutoSize = true;
            this.totalPriceLabel.Location = new System.Drawing.Point(127, 291);
            this.totalPriceLabel.Name = "totalPriceLabel";
            this.totalPriceLabel.Size = new System.Drawing.Size(82, 15);
            this.totalPriceLabel.TabIndex = 10;
            this.totalPriceLabel.Text = "Total: 0.00CHF";
            // 
            // itemsList
            // 
            this.itemsList.AutoSize = true;
            this.itemsList.Location = new System.Drawing.Point(36, 341);
            this.itemsList.Name = "itemsList";
            this.itemsList.Size = new System.Drawing.Size(0, 15);
            this.itemsList.TabIndex = 11;
            // 
            // mozza
            // 
            this.mozza.Location = new System.Drawing.Point(166, 83);
            this.mozza.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.mozza.Name = "mozza";
            this.mozza.Size = new System.Drawing.Size(79, 23);
            this.mozza.TabIndex = 12;
            this.mozza.ValueChanged += new System.EventHandler(this.mozza_ValueChanged);
            // 
            // spinach
            // 
            this.spinach.Location = new System.Drawing.Point(166, 109);
            this.spinach.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.spinach.Name = "spinach";
            this.spinach.Size = new System.Drawing.Size(79, 23);
            this.spinach.TabIndex = 13;
            this.spinach.ValueChanged += new System.EventHandler(this.spinach_ValueChanged);
            // 
            // mushroom
            // 
            this.mushroom.Location = new System.Drawing.Point(166, 135);
            this.mushroom.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.mushroom.Name = "mushroom";
            this.mushroom.Size = new System.Drawing.Size(79, 23);
            this.mushroom.TabIndex = 14;
            this.mushroom.ValueChanged += new System.EventHandler(this.mushroom_ValueChanged);
            // 
            // ham
            // 
            this.ham.Location = new System.Drawing.Point(166, 160);
            this.ham.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.ham.Name = "ham";
            this.ham.Size = new System.Drawing.Size(79, 23);
            this.ham.TabIndex = 15;
            this.ham.ValueChanged += new System.EventHandler(this.ham_ValueChanged);
            // 
            // olives
            // 
            this.olives.Location = new System.Drawing.Point(404, 160);
            this.olives.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.olives.Name = "olives";
            this.olives.Size = new System.Drawing.Size(77, 23);
            this.olives.TabIndex = 19;
            this.olives.ValueChanged += new System.EventHandler(this.olives_ValueChanged);
            // 
            // chicken
            // 
            this.chicken.Location = new System.Drawing.Point(404, 135);
            this.chicken.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.chicken.Name = "chicken";
            this.chicken.Size = new System.Drawing.Size(77, 23);
            this.chicken.TabIndex = 18;
            this.chicken.ValueChanged += new System.EventHandler(this.chicken_ValueChanged);
            // 
            // pineapple
            // 
            this.pineapple.Location = new System.Drawing.Point(404, 109);
            this.pineapple.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.pineapple.Name = "pineapple";
            this.pineapple.Size = new System.Drawing.Size(77, 23);
            this.pineapple.TabIndex = 17;
            this.pineapple.ValueChanged += new System.EventHandler(this.pineapple_ValueChanged);
            // 
            // pepperoni
            // 
            this.pepperoni.Location = new System.Drawing.Point(404, 83);
            this.pepperoni.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.pepperoni.Name = "pepperoni";
            this.pepperoni.Size = new System.Drawing.Size(77, 23);
            this.pepperoni.TabIndex = 16;
            this.pepperoni.ValueChanged += new System.EventHandler(this.pepperoni_ValueChanged);
            // 
            // pizzaPrice
            // 
            this.pizzaPrice.AutoSize = true;
            this.pizzaPrice.Location = new System.Drawing.Point(230, 245);
            this.pizzaPrice.Name = "pizzaPrice";
            this.pizzaPrice.Size = new System.Drawing.Size(89, 15);
            this.pizzaPrice.TabIndex = 20;
            this.pizzaPrice.Text = "Price: 12.00CHF";
            // 
            // addToCartBtn
            // 
            this.addToCartBtn.Location = new System.Drawing.Point(231, 219);
            this.addToCartBtn.Name = "addToCartBtn";
            this.addToCartBtn.Size = new System.Drawing.Size(75, 23);
            this.addToCartBtn.TabIndex = 21;
            this.addToCartBtn.Text = "Add to cart";
            this.addToCartBtn.UseVisualStyleBackColor = true;
            this.addToCartBtn.Click += new System.EventHandler(this.addToCartBtn_Click);
            // 
            // orderBtn
            // 
            this.orderBtn.Location = new System.Drawing.Point(393, 291);
            this.orderBtn.Name = "orderBtn";
            this.orderBtn.Size = new System.Drawing.Size(75, 22);
            this.orderBtn.TabIndex = 22;
            this.orderBtn.Text = "Order";
            this.orderBtn.UseVisualStyleBackColor = true;
            this.orderBtn.Click += new System.EventHandler(this.orderBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 1012);
            this.Controls.Add(this.orderBtn);
            this.Controls.Add(this.addToCartBtn);
            this.Controls.Add(this.pizzaPrice);
            this.Controls.Add(this.olives);
            this.Controls.Add(this.chicken);
            this.Controls.Add(this.pineapple);
            this.Controls.Add(this.pepperoni);
            this.Controls.Add(this.ham);
            this.Controls.Add(this.mushroom);
            this.Controls.Add(this.spinach);
            this.Controls.Add(this.mozza);
            this.Controls.Add(this.itemsList);
            this.Controls.Add(this.totalPriceLabel);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Pizza\'Orderator";
            ((System.ComponentModel.ISupportInitialize)(this.mozza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mushroom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.olives)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chicken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pineapple)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pepperoni)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label label10;
        private Label totalPriceLabel;
        private Label itemsList;
        private NumericUpDown mozza;
        private NumericUpDown spinach;
        private NumericUpDown mushroom;
        private NumericUpDown ham;
        private NumericUpDown olives;
        private NumericUpDown chicken;
        private NumericUpDown pineapple;
        private NumericUpDown pepperoni;
        private Label pizzaPrice;
        private Button addToCartBtn;
        private Button orderBtn;
    }
}