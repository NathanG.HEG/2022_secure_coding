namespace PizzaOrderator
{
    public partial class Form1 : Form
    {
        private List<Pizza> order;
        private List<NumericUpDown> extras;
        private readonly int EXTRA_COST = 250;
        private int totalPrice;
        public Form1()
        {
            order = new List<Pizza>();
            InitializeComponent();
            extras = new List<NumericUpDown>()
                {mozza, spinach, chicken, ham, mushroom, pepperoni, olives, pineapple};
        }

        private void addToCartBtn_Click(object sender, EventArgs e)
        {
            if (order.Count >= 20)
            {
                return;
            }
            Pizza pizza = new Pizza(order.Count+1);
            foreach (var extra in extras)
            {
                pizza.priceInCent += (int) extra.Value * EXTRA_COST;
            }

            totalPrice += pizza.priceInCent;
            int r = totalPrice / 100;
            int cents = totalPrice - r*100;
            totalPriceLabel.Text = "Total: " + r + "." + cents + "CHF";

            addToOrder(pizza);

            // Fictive method
            sendToPizzaiolo(pizza);
        }

        private void updatePrice()
        {
            int price = 1200;
            foreach (var extra in extras)
            {
                price += (int) extra.Value * EXTRA_COST;
            }

            int r = price / 100;
            int cents = price - r*100;
            pizzaPrice.Text = "Price: " + r+"."+cents+ "CHF";
        }

        private void addToOrder(Pizza pizza)
        {
            order.Add(pizza);
            itemsList.Text = itemsList.Text.Insert(itemsList.Text.Length, pizza.ToString() + "\n");
        }
        private void orderBtn_Click(object sender, EventArgs e)
        {
            if(order.Count == 0) return;
            string message;
            if (totalPrice > 0)
            {
                int r = totalPrice / 100;
                int cents = totalPrice - r * 100;
                message = "Thanks, please pay " + r + "." + cents + "CHF at the counter";
            }
            else
            {
                message = "Thanks, your order has already been paid! Pick up your pizza at the counter";
            }
            this.Visible = false;
            this.IsMdiContainer = true;
            Form2 form2 = new Form2(message, this);
            form2.Show();
            
        }

        private void sendToPizzaiolo(Pizza p)
        {
            // sends pizza to chef
        }
        private void mozza_ValueChanged(object sender, EventArgs e)
        {
            updatePrice();
        }

        private void spinach_ValueChanged(object sender, EventArgs e)
        {
            updatePrice();
        }

        private void mushroom_ValueChanged(object sender, EventArgs e)
        {
            updatePrice();
        }

        private void ham_ValueChanged(object sender, EventArgs e)
        {
            updatePrice();
        }

        private void pepperoni_ValueChanged(object sender, EventArgs e)
        {
            updatePrice();
        }

        private void pineapple_ValueChanged(object sender, EventArgs e)
        {
            updatePrice();
        }

        private void chicken_ValueChanged(object sender, EventArgs e)
        {
            updatePrice();
        }

        private void olives_ValueChanged(object sender, EventArgs e)
        {
            updatePrice();
        }
    }
}