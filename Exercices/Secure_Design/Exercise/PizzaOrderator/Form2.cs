﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaOrderator
{
    public partial class Form2 : Form
    {
        private Form parent;
        public Form2(string message, Form parent)
        {
            InitializeComponent();
            this.message.Text = message;
            Visible = true;
            this.parent = parent;
        }

        private void message_Click(object sender, EventArgs e)
        {

        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            parent.Dispose();
            Dispose();
        }
    }
}
