﻿namespace PizzaOrderator;

public class Pizza
{
    public int priceInCent { get; set; }
    public int id { get; set; }
    public Pizza(int id)
    {
        this.id = id;
        priceInCent = 1200;
    }
    public override string ToString()
    {
        float price = ((float) priceInCent) / 100;
        if (id < 10)
        {
            return "Pizza " + id + "                " + price + "CHF";
        }
        return "Pizza " + id + "              " + price + "CHF";
    }
}