# Secure Design

PizzaOrderator is a simple program that simulates a pizzeria order.
The pizzaiolo watched a Youtube tutorial to create simple GUI C# program.
Compose your pizza by adding elements and add it to the cart.
The total price is then asked to be paid at the counter.

To avoid misuse, the pizzaiolo-developer decided to limit the quantity 
of pizzas to 20 per order. 

1. How can users still misuse the program?
1. Can you manage to get free pizzas? How?
1. Generally speaking, is it a good idea to build such solutions as a non-expert in a business environment? What are the pros and cons?



## How to run
### Install .NET 6.0 SDK
Check your .NET SDK
```bash
dotnet --info
```

If your version is older than 6.0, download and install .NET 6.0 from [Microsoft](https://dotnet.microsoft.com/en-us/download/dotnet/6.0).

### Compile and run
```bash
cd PizzaOrderator
dotnet build
cd /bin/debug/net6.0-windows
PizzaOrderator.exe
```