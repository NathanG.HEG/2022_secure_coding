import Exceptions.BusinessException;
import Exceptions.ErrorCode;
import File.FileManager;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class FileManagerTest {

    private final static String READ_TEST_FILE_PATH = "read_test.txt";
    private final static String WRITE_TEST_FILE_PATH = "write_test.txt";

    @Test
    public void testReadFile() {
        FileManager fileManager = new FileManager();
        try {
            assertEquals(fileManager.getFileContent(READ_TEST_FILE_PATH), "hello world\n");
        } catch (BusinessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testWriteFile() {

        //prepare content
        String content = "Hello,\nHappy friday\nBenjamin";

        //write in file
        FileManager fileManager = new FileManager();
        try {
            fileManager.setFileContent(WRITE_TEST_FILE_PATH, content);
        } catch (BusinessException e) {
            e.printStackTrace();
        }

        //read file content
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(WRITE_TEST_FILE_PATH));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null)
                sb.append(line).append("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //compare written value and read value
        assertEquals(content + "\n", sb.toString());
    }

    @Test
    public void testReadUnknownFile() {
        FileManager fileManager = new FileManager();
        try {
            fileManager.getFileContent("non_existing_file.txt");
        } catch (BusinessException e) {
            assertEquals(e.getErrorCode(), ErrorCode.UNKNOWN_FILE);
        }
    }


}
