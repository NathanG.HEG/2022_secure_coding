# Unit testing in Java

## Answers

Unit tests for the question 1 to 3 are provided in *FileManagerTest.java.*
This is just a possible version and not an universal solution.

<strong> 2 . Test the write method. Can you rely on the read method to test this method ? </strong>

Even though it would be more convenient, you shouln't use the read method to test the write method.
That's because unit tests assess individually independant pieces of code. 
When relying on the read method, if the read method fails, the write test will fail too.
Consequently, it is harder to identify the source of failure.

## Troubleshoot

Because this project is dealing with files, permissions can be a problem.
If the file manager cannot write in files, try: 
- Disabling your anti-virus.
- Changing filesystem permissions.




