# Unit testing in Java


As their name suggests, unit tests are assessing units of software that can be reasonably isolated. 
Those units can be methods, subroutines, objects, or modules. 
Unit testing is the first level of functional testing and should be performed as early as possible in the development lifecycle. 

## Prequisites
-  [Java 8+](https://www.oracle.com/java/technologies/javase/jdk-relnotes-index.html)

## Exercise

Open the provided Java project "Simple_File_Manager".
This file manager has two methods: it can read and write in .txt files.
For the following tasks, use the existing "src/test/FileManagerTest" class and [JUnit](https://junit.org/junit5/docs/current/user-guide/) testing engine. 
JUnit required dependencies have already been added to the project's pom.xml file.

1. Test the read method.
2. Test the write method. Can you rely on the read method to test this method ?
3. Ensure the good error code is returned when trying to read a non-existing file.

A version of working test cases is provided within answer folder.






    