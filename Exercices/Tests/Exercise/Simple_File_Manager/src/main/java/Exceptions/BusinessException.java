package Exceptions;


public class BusinessException extends Exception {

    ErrorCode errorCode;

    public BusinessException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCodeInt() {
        return errorCode.getCode();
    }

    public ErrorCode getErrorCode(){
        return errorCode;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

}
