package Exceptions;

public enum ErrorCode {

    UNKNOWN_FILE(1),
    OPERATION_FAILED(2),
    IO_ERROR(3);


    private final int code;

    ErrorCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}

