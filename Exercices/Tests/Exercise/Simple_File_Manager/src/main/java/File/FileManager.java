package File;

import Exceptions.BusinessException;
import Exceptions.ErrorCode;

import java.io.*;

public class FileManager {

    public String getFileContent(String filePath) throws BusinessException {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
          throw new BusinessException("Unknown file.", ErrorCode.UNKNOWN_FILE);
        }

        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null)
            sb.append(line).append("\n");
        } catch (IOException e) {
            throw new BusinessException("Read operation failed.", ErrorCode.OPERATION_FAILED);
        }
        return sb.toString();
    }

    public void setFileContent(String filePath, String content) throws BusinessException  {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(filePath));
        } catch (IOException e) {
            throw new BusinessException("IO error occurred while initiating writing operation.", ErrorCode.IO_ERROR);
        }

        try {
            writer.write(content);
            writer.close();
        } catch (IOException e) {
            throw new BusinessException("IO error occureed during write operation.", ErrorCode.IO_ERROR);
        }
    }
}
