package Runnable;

import Exceptions.BusinessException;
import File.FileManager;

public class Run {
    public static void main(String[] args) {
        FileManager fileManager = new FileManager();
        try {
            fileManager.setFileContent("read_test.txt", "hello world");
            System.out.println(fileManager.getFileContent("read_test.txt"));
        } catch (BusinessException e) {
            System.err.println(e.getMessage());
        }
    }
}
