# Secure Coding

## Purpose
If cybercrime damage was considered a country, it would be the world’s third largest economy after the Unites States and China. Indeed, digital attacks have costed six trillion US dollars in 2021. What’s more, developers will need to secure 338 billion lines of code in 2025. This represents a 15 percent augmentation per year (Morgan, 2022). Cybercrime thrives on the growing digitalization. Sensitive data, including banking details and medical information are plentiful. In this context, it is essential for IT students to learn about secure coding.
In short, secure coding is a set of best practices in order to prevent system vulnerabilities. Most of these vulnerabilities come from common programming or design errors. Consequently, identifying and correcting these errors enhances information system’s reliability and security. This document lists and explains some of the common existing vulnerabilities. Obviously, not all threats are treated within this report, and developers should be aware that new breaches are constantly uncovered. 

### Key words
cybersecurity, coding, vulnerabilities, continuous integration, continuous delivery


## Recommended environment
- Development: Java, C# and C IDEs (e.g. Jetbrains IntelliJ, Microsoft Visual Studio 2022)
- Run: Java 11+, .NET 6.0, a C compiler

## Authors
- Authors: Biollaz Benjamin, Choffat Théo, Gaillard Nathan
- Contributors: Beuchat Jean-Luc

## Licence
Developed and redacted in academic context at HES-SO for educational purpose only.